public class RunMotel {
    public static void main(String[] args) {
        Motel motel1 = new Motel();
        motel1.getMotel();

        // Payments
        CarfardPal c1 = new CarfardPal("Super Cafard", 123456789);
        MasterCafard m1 = new MasterCafard("Cool Cafard", 902132, 1212, "03-21");
        CarfardPal c2 = new CarfardPal("Best Cafarette", 213467890);

        // Customers
        System.out.println("========= Customers =========");
        Customer customer1 = new Customer(101, "Super Cafard", 5, 0.3, c1);
        Customer customer2 = new Customer(102, "Cool Cafard", 20, 0.1, m1);
        Customer customer3 = new Customer(103, "Star Cafard", 1, 1.5, m1);
        Customer customer4 = new Customer(104, "Top Cafarette", 10, 0.4, c2);
        Customer customer5 = new Customer(105, "Best Cafarette", 33, 0.5, c2);
        Customer customer6 = new Customer(106, "Intelo Cafarette", 4, 0.1, m1);
        Customer customer7 = new Customer(107, "Geek Cafard", 2, 1.5, m1);
        Customer customer8 = new Customer(108, "Creative Cafarette", 10, 0.4, c2);
        Customer customer9 = new Customer(109, "Bosseur Cafard", 22, 2, c2);
        Customer customer10 = new Customer(110, "Grincheux Cafarette", 13, 2, c2);

        customer1.affichage();
        customer2.affichage();
        customer3.affichage();
        customer4.affichage();
        customer5.affichage();
        customer6.affichage();
        customer7.affichage();
        customer8.affichage();
        customer9.affichage();
        customer10.affichage();

        // Rooms
        System.out.println("========= Rooms =========");
        RoomPackage Suite1 = new Suite(3, 223, false, 2, customer2);
        RoomPackage Suite2 = new Suite(3, 224, false, 2, customer3);
        RoomPackage Standard1 = new Standard(2, 101, false, 8, customer4);
        RoomPackage Standard2 = new Standard(2, 102, false, 8, customer10);
        // RoomPackage Standard3 = new Standard(2, 103);
        // RoomPackage Standard4 = new Standard(2, 104);
        // RoomPackage Standard5 = new Standard(2, 105);
        // RoomPackage Luxe1 = new Luxe(1, 601);
        RoomPackage Luxe2 = new Luxe(1, 602, false, 5, customer5);
        RoomPackage Luxe3 = new Luxe(1, 603, false, 5, customer6);
        RoomPackage L1 = new Luxe(1, 602, false, 5, customer5);
        RoomPackage L2 = new Luxe(1, 603, false, 5, customer6);
        RoomPackage L3 = new Luxe(1, 602, false, 5, customer5);
        RoomPackage L4 = new Luxe(1, 603, false, 5, customer6);
        RoomPackage L5 = new Luxe(1, 602, false, 5, customer5);
        RoomPackage L6 = new Luxe(1, 603, false, 5, customer6);
        RoomPackage L7 = new Luxe(1, 602, false, 5, customer5);
        RoomPackage L8 = new Luxe(1, 603, false, 5, customer6);
        RoomPackage L9 = new Luxe(1, 602, false, 5, customer5);
        RoomPackage L10 = new Luxe(1, 603, false, 5, customer6);

        // Comoditiés
        Suite1 = new Spa(Suite1);
        Suite1 = new Shower(Suite1);
        Suite1 = new MiniBar(Suite1);
        Suite2 = new Spa(Suite2);
        Suite2 = new Shower(Suite2);
        Standard1 = new Shower(Standard1);
        // Standard1 = new Shower(Standard1);
        Standard1 = new MiniBar(Standard1);
        // Luxe1 = new MiniBar(Luxe1);

        // Add rooms
        for (int i = 0; i < Motel.numberRooms; i++) {
            if (i < 1) {

                Motel.listRooms.add(Suite1);
                Motel.listRooms.add(Suite2);
                Motel.listRooms.add(Standard1);
                Motel.listRooms.add(Standard2);
                // Motel.listRooms.add(Standard3);
                // Motel.listRooms.add(Standard4);
                // Motel.listRooms.add(Standard5);
                // Motel.listRooms.add(Luxe1);
                Motel.listRooms.add(Luxe2);
                Motel.listRooms.add(Luxe3);
                Motel.listRooms.add(L1);
                Motel.listRooms.add(L2);
                Motel.listRooms.add(L3);
                Motel.listRooms.add(L4);
                Motel.listRooms.add(L5);
                Motel.listRooms.add(L6);
                Motel.listRooms.add(L7);
                Motel.listRooms.add(L8);
                Motel.listRooms.add(L9);
                Motel.listRooms.add(L10);
            }
        }

        Motel.listRooms.size();
        motel1.getDisponibility();
        motel1.getStatus();

        System.out.println(" Number of rooms: " + motel1.getNumberRooms());

        // Affichage de la liste des chambres
        System.out.println("list of rooms: \n" + Motel.listRooms.toString());

        // affiche toutes les factures
        for (RoomPackage elem : Motel.listRooms) {
            elem.printBill();
            elem.printTotal();
        }

        // inscription
        customer1.subscribe(motel1);
        customer2.subscribe(motel1);
        customer3.subscribe(motel1);

        motel1.free();
        motel1.busy();

    }
}