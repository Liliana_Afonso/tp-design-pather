public interface MyObserver {

    public void notifier(boolean status);

    public void subscribe(Motel M);
}