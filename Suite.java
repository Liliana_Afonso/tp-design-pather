public class Suite extends Room {
    public Suite(int id, int roomName, boolean status, int night, Customer cafard) {
        this.id = id;
        this.roomName = roomName;
        this.status = status;
        this.night = night;
        this.cafard = cafard;
    }

    public Suite(int id, int roomName) {
        this.id = id;
        this.roomName = roomName;
    }

    @Override
    double price() {
        return 100 * getNight();
    }

    @Override
    void printBill() {
        super.printBill();
        System.out.println("Suite n°" + getRoomName() + ":\t\t" + getNight() + "days\t100.- \t" + price());
    }

}
