public class MiniBar extends Options {

    public MiniBar(RoomPackage roomPackage) {
        super(roomPackage);
    }

    @Override
    double price() {
        return 10 + this.roomPackage.price();
    }

    @Override
    void printBill() {
        this.roomPackage.printBill();
        System.out.println("Mini Bar:\t\t\t\t10.0");
    }

}