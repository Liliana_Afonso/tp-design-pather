public class Payment {
    public int id;
    public String name;
    public int carteCode;

    public int getId() {
        return id;

    }

    public void setId(int id) {
        this.id = id;

    }

    public String getName() {
        return name;

    }

    public void setName(String name) {
        this.name = name;

    }

    public int getCarteCode() {
        return carteCode;

    }

    public void setCarteCode(int carteCode) {
        this.carteCode = carteCode;

    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "\tType of payment: \t" + getClass().getSimpleName();
    }

}