public class Standard extends Room {

    public Standard(int id, int roomName, boolean status, int night, Customer cafard) {
        this.id = id;
        this.roomName = roomName;
        this.status = status;
        this.night = night;
        this.cafard = cafard;
    }

    public Standard(int id, int roomName) {
        this.id = id;
        this.roomName = roomName;
    }

    @Override
    double price() {
        return 50 * getNight();
    }

    @Override
    void printBill() {
        super.printBill();
        System.out.println("Standard n°" + getRoomName() + ":\t\t" + getNight() + "days\t50.- \t" + price());
    }

}
