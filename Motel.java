import java.util.List;
import java.util.ArrayList;

public class Motel {
    static int id;
    static int capacity;
    List<MyObserver> listObservers;
    static boolean status; // Busy or Free
    static ArrayList<RoomPackage> listRooms = new ArrayList<RoomPackage>();
    static int numberRooms = 10;

    public int getNumberRooms() {
        return numberRooms;
        // return Motel.listRooms.size();
        // return Motel.listRooms.size(); // pour retourner le nombre de chambres
        // présentes dans l'hotel
    }

    public void setNumberRooms(int numberRooms) {
        this.numberRooms = numberRooms;
    }

    public ArrayList<RoomPackage> getListRooms() {
        return listRooms;
    }

    public void setListRooms(ArrayList<RoomPackage> listRooms) {
        this.listRooms = listRooms;
    }

    private Motel myMotel;

    public Motel getMotel() {
        if (myMotel == null) {
            myMotel = new Motel();
            System.out.println("Bienvenue au Motel de cafards");
        }
        return myMotel;
    }

    public Motel() {
        this.listObservers = new ArrayList<MyObserver>();
        this.status = true;
    }

    public void free() {
        this.status = true;
        this.notifyAllObservers();
    }

    public void busy() {
        this.status = false;
        this.notifyAllObservers();
    }

    public void subscribe(MyObserver newObserver) {
        this.listObservers.add(newObserver);
    }

    public void notifyAllObservers() {
        for (MyObserver myObserver : listObservers) {
            myObserver.notifier(this.status);
        }
    }

    public void getDisponibility() {
        for (RoomPackage room : listRooms) {
            System.out.println(room);

            if (room.price() > 300) { // On aurait voulu getStatus des room mais on a accès que au prix via
                                      // roomPackage
                this.status = true;
                this.notifyAllObservers();

            } else {
                this.status = false;
                this.notifyAllObservers();
            }
        }

    }

    public void getStatus() {
        if (status == true) {
            System.out.println("Motel Free");

        } else {
            System.out.println("Motel Busy");
        }
    }

}