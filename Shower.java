public class Shower extends Options {

    public Shower(RoomPackage roomPackage) {
        super(roomPackage);
    }

    @Override
    double price() {
        return 25 + this.roomPackage.price();
    }

    @Override
    void printBill() {
        this.roomPackage.printBill();
        System.out.println("Shower:\t\t\t\t\t25.0");
    }

}