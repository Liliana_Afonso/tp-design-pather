abstract class Options extends RoomPackage {
    static int id;
    static Double price;
    RoomPackage roomPackage;

    public Options(RoomPackage roomPackage) {
        if (isUnique(roomPackage)) {

            this.roomPackage = roomPackage;
        }
        roomPackage = this;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return roomPackage.toString();
    }

    public boolean isUnique(RoomPackage roomPackage) {

        return roomPackage.getClass() != this.getClass(); // return spa
    }

    // Methode pour calculer le nombre de dècées causé par Insecticide
    // public void getInsecticide() {
    // if (this.getClass() == Shower) {
    // (double) cafard.getRateOfGrowth() * (double) cafard.getPopulationInitial() *
    // 0.50;

    // }
    // else {

    // (double) cafard.getRateOfGrowth() * (double) cafard.getPopulationInitial() *
    // 0.75;
    // }

    // }

    // Méthode pour get les chammbres
    // public void getRoom() {}
}