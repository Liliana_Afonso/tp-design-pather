abstract class RoomPackage {
    abstract double price();

    void printBill() {
        System.out.println("\n\n=================== Facture ===================\n");
    }

    void printTotal() {
        System.out.println("-----------------------------------------------\nTotal:\t\t\t\t\t" + this.price());
    }
}