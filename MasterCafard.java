public class MasterCafard extends Payment {
    public int securityCode;
    public String expiredDate;

    public MasterCafard(String name, int carteCode, int securityCode, String expiredDate) {
        this.name = name;
        this.carteCode = carteCode;
        this.securityCode = securityCode;
        this.expiredDate = expiredDate;
    }

}
