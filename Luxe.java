public class Luxe extends Room {

    public Luxe(int id, int roomName, boolean status, int night, Customer cafard) {
        this.id = id;
        this.roomName = roomName;
        this.status = status;
        this.night = night;
        this.cafard = cafard;

    }

    public Luxe(int id, int roomName) {
        this.id = id;
        this.roomName = roomName;
    }

    // public Luxe() {
    // super();
    // }

    @Override
    double price() {
        return 75 * getNight();
    }

    @Override
    void printBill() {
        super.printBill();
        System.out.println("Luxe n°" + getRoomName() + ":\t\t" + getNight() + "days\t75.- \t" + price());
    }

}
