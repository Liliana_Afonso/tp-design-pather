
public abstract class Room extends RoomPackage {
    protected int id;
    protected double price;
    protected int roomName;
    protected boolean status;
    protected int night;
    protected Customer cafard;

    // On a testé de faire avec la méthode que tu as dit mais on n'a pas réussit
    // public Room(int id, int roomName, boolean status, int night, Customer cafard)
    // {
    // this.id = 0;
    // this.roomName = 0;
    // this.status = true;
    // this.night = 0;
    // this.cafard = null;
    // }

    // public Room(int id, int roomName) {
    // this.id = 0;
    // this.roomName = 0;
    // }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " Room N° : " + roomName + " Price : " + getPrice() + " Status : "
                + status + " Days : " + night + " Customer : " + cafard.getName() + "\n";
    }

    public int getNight() {
        return night;

    }

    public void setNight(int night) {
        this.night = night;

    }

    public Customer getCafard() {
        return cafard;

    }

    public void setCafard(Customer cafard) {
        this.cafard = cafard;

    }

    public int getId() {
        return id;

    }

    public void setId(int id) {
        this.id = id;

    }

    public double getPrice() {
        return price();

    }

    public void setPrice(double price) {
        this.price = price;

    }

    public int getRoomName() {
        return roomName;

    }

    public void setRoomName(int roomName) {
        this.roomName = roomName;

    }

    public boolean getStatus() {
        return status;

    }

    public void setStatus(boolean status) {
        this.status = status;

    }

}