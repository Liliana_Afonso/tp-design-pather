/**
 * Customer
 */
public class Customer implements MyObserver {
    private int id;
    private String name;
    private int populationInitial;
    private double rateOfGrowth;
    private Payment paymentType;

    public Customer(int id, String name, int populationInitial, double rateOfGrowth, Payment paymentType) {
        this.id = id;
        this.name = name;
        this.populationInitial = populationInitial;
        this.rateOfGrowth = rateOfGrowth;
        this.paymentType = paymentType;
    }

    public Payment getPaymentType() {
        return paymentType;

    }

    public void setPaymentType(Payment paymentType) {
        this.paymentType = paymentType;

    }

    public int getId() {
        return id;

    }

    public void setId(int id) {
        this.id = id;

    }

    public String getName() {
        return name;

    }

    public void setName(String name) {
        this.name = name;

    }

    public int getPopulationInitial() {
        return populationInitial;

    }

    public void setPopulationInitial(int populationInitial) {
        this.populationInitial = populationInitial;

    }

    public double getRateOfGrowth() {
        return rateOfGrowth;

    }

    public void setRateOfGrowth(double rateOfGrowth) {
        this.rateOfGrowth = rateOfGrowth;

    }

    @Override
    public void notifier(boolean status) {
        System.out.println(status ? "client rentre" : "client attend");
    }

    public void subscribe(Motel M) {
        M.subscribe(this);
    }

    public void affichage() {
        System.out.println("name: \t" + name + "\t Number of people: \t" + populationInitial + "\t Rate Of Growth:\t "
                + rateOfGrowth + paymentType.toString());

    }

}