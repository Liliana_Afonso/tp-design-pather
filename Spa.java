public class Spa extends Options {

    public Spa(RoomPackage roomPackage) {
        super(roomPackage);
    }

    @Override
    double price() {
        return 20 + this.roomPackage.price(); // we get room price & spa price
    }

    @Override
    void printBill() {
        this.roomPackage.printBill();
        System.out.println("Spa:\t\t\t\t\t20.0");
    }

}