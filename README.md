# Documentation du projet

## Singleton

Nous avons choisi de définir Motel comme étant notre Singleton car c'est un objet qui doit être instancié une seule fois.

## Design patern Observer

Ensuite, nous avons choisi le design patern Observer pour Customer car c'est une relation ONE-TO-MANY avec le Motel; On peut avoir plusieurs clients dans un Motel mais les clients ne peuvent aller que dans un Motel. De plus, le clients veulent être notifiés lorsque le Motel est libre et ce design pathern nous permet de faire ça.

## Design patern Decorator

Concernant le design patern Decorator, nous avons utilisé pour représenter les commodités du Motel soient; Spa, Douche et Mini bar, car on ce pathern permet de ajouter des nouvelles fonctionnalités à un objet (dans notre cas à RoomPackage qui contients Room)

## Design patern Abstract Factory

Finalement, nous avons choisi le design patern Abstract Factory pour représenter Room qui contient Luxe, Standard et Suite car ce patern permet de "créer une familles entière de produit sans spécifié leur classe concète".

## Inferface

Nous avons une interface qui est MyObserver qui va notifier les changements aux clients lorsque que le Motel est occupé ou libre.
